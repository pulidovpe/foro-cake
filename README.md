# Foro-Cake
> Un foro sencillo hecho en Cakephp 2.6 usando una base de datos en Mysql.
> Desarrollado únicamente con fines didácticos.

## Screenshots / Capturas de Pantalla


## Tech-framework used / Tecnologías Usadas
- PHP5
- CakePhp 2.x
- MySQL-Server

## Requirements / Requisitos
- Servidor Web (Apache preferiblemente).
- PHP5.
- Servidor MySQL.

## Install / Instalación
#### OS X, Linux y Windows
*Abra un terminal y ejecute:*
```Shell
cd [ruta_servidor_web]

git clone http://gitlab.com/pulidovpe/DptoArchivo.git

cd DptoArchivo
```

## Tasks / Lista de Tareas
- [x] Inicializar repositorio
- [x] Subir mis cambios a GitLab
- [x] Completar el back-end y el front-end

## License / Licencia
Pulido V.P.E. – @gitlab/pulidovpe – pulidovpe.dev@gmail.com
Distributed under the MIT license. See [LICENSE](LICENSE) for more information.

## CakePHP

[![Latest Stable Version](https://poser.pugx.org/cakephp/cakephp/v/stable.svg)](https://packagist.org/packages/cakephp/cakephp)
[![License](https://poser.pugx.org/cakephp/cakephp/license.svg)](https://packagist.org/packages/cakephp/cakephp)
[![Bake Status](https://secure.travis-ci.org/cakephp/cakephp.png?branch=master)](http://travis-ci.org/cakephp/cakephp)
[![Code consistency](http://squizlabs.github.io/PHP_CodeSniffer/analysis/cakephp/cakephp/grade.svg)](http://squizlabs.github.io/PHP_CodeSniffer/analysis/cakephp/cakephp/)

[![CakePHP](http://cakephp.org/img/cake-logo.png)](http://www.cakephp.org)

CakePHP is a rapid development framework for PHP which uses commonly known design patterns like Active Record, Association Data Mapping, Front Controller and MVC.
Our primary goal is to provide a structured framework that enables PHP users at all levels to rapidly develop robust web applications, without any loss to flexibility.

## Contributing

[CONTRIBUTING.md](CONTRIBUTING.md) - Quick pointers for contributing to the CakePHP project

[CookBook "Contributing" Section (2.x)](http://book.cakephp.org/2.0/en/contributing.html) [(3.0)](http://book.cakephp.org/3.0/en/contributing.html) - Version-specific details about contributing to the project
